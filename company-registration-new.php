<!DOCTYPE HTML>
  <html class="no-js" lang="de">  
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="keywords" content="">

    <title>Bootsnav - Bootstrap menu multi purpose header</title>

    <!-- CSS -->
    <link href="new-landingCss/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css" rel="stylesheet">
     <link href="new-landingCss/animate.css" rel="stylesheet">
    <link href="new-landingCss/bootsnav.css" rel="stylesheet">
    <link href="new-landingCss/style.css" rel="stylesheet">
    <link href="new-landingCss/animations.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
   </head>
<body>     
        
    <!-- Start Navigation -->
    <nav class="navbar navbar-default bootsnav navbar-fixed-top">

        <!-- Start Top Search -->
        <div class="top-search">
            <div class="container">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                </div>
            </div>
        </div>
        <!-- End Top Search -->

        <div class="container">            
            <!-- Start Atribute Navigation -->
             

            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#brand"><img src="https://www.caonweb.com/expert-images/logo4.png" class="logo" alt=""></a>
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li class="dropdown megamenu-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Services</a>
                        <ul class="dropdown-menu megamenu-content" role="menu">
                            <li>
                                <div class="row">
                                    <div class="col-menu col-md-4">
                                        <h6 class="title">Service 1</h6>
                                        <div class="content">
                                          
											<ul class="menu-col">
														<li><a href="https://www.caonweb.com/tds-return.php" target="_blank">TDS Return</a></li>
														<li><a href="https://www.caonweb.com/msme-ssi-registration.php" target="_blank">MSME Registration</a></li>
														<li><a href="https://www.caonweb.com/import-export-code.php" target="_blank">Import Export Code</a></li>
														<li><a href="https://www.caonweb.com/gst-registration.php" target="_blank">GST Registration</a></li>
														<li><a href="https://caonweb.com/gst-filing.php" target="_blank">GST Filing</a></li>
														<li><a href="https://www.caonweb.com/digital-signature-certificate.php" target="_blank">Digital Signature Certificates</a></li>
														<li><a href="https://www.caonweb.com/audit-and-assurance-services.php" target="_blank">Audit and Assurance Services</a></li>
													</ul>
                                        </div>
                                    </div><!-- end col-3 -->
                                    <div class="col-menu col-md-4">
                                        <h6 class="title">Service 2</h6>
                                        <div class="content">
                                            
											<ul class="menu-col">
													
														<li><a href="https://www.caonweb.com/company-registration.php" target="_blank">Company Registration</a></li>
														<li><a href="https://www.caonweb.com/book-keeping-and-outsourcing.php" target="_blank">Book Keeping &amp; Outsourcing</a></li>
														<li><a href="https://www.caonweb.com/apeda-registration.php" target="_blank">APEDA Registration</a></li>
														<li><a href="https://www.caonweb.com/fssai-registration.php" target="_blank">FSSAI Registration</a></li>
														<li><a href="https://www.caonweb.com/trademark-registration.php" target="_blank">Trademark Registration</a></li>
														<li><a href="https://www.caonweb.com/income-tax-return.php" target="_blank">Income Tax Return</a></li>
														<li><a href="https://www.caonweb.com/doing-business-in-india.php" target="_blank">Doing Business in India</a></li>
														<li><a href="https://www.caonweb.com/warehouse-registration.php" target="_blank">Warehouse Registration</a></li>
														
														
													</ul>
                                        </div>
                                    </div><!-- end col-3 -->
                                    <div class="col-menu col-md-4">
                                        <h6 class="title">Service 3</h6>
                                        <div class="content">
                                           
											<ul class="menu-col">
														<li><a href="https://www.caonweb.com/cross-border-transaction.php" target="_blank">Cross Boarder Transaction- Structuring &amp; Taxation</a></li>
														<li><a href="https://www.caonweb.com/fdi-in-india.php" target="_blank">FDI in India</a></li>
														<li><a href="https://www.caonweb.com/nri-services.php" target="_blank">NRI Services</a></li>
														<li><a href="https://www.caonweb.com/iso-registration.php" target="_blank">ISO Registration</a></li>
														<li><a href="https://www.caonweb.com/ngo-services.php" target="_blank">NGO Services</a></li>
													    <li><a href="https://www.caonweb.com/company-secretarial-services.php" target="_blank">Compliance by company</a></li>
													    <li><a href="https://www.caonweb.com/annual-filing.php" target="_blank">ROC / Annual Filling</a></li>
													    <li><a href="https://www.caonweb.com/ca-services.php" target="_blank">All CA Services</a></li>
														 <!--<li><a href="https://www.caonweb.com/importer-of-record-service.php" target="_blank">Importer of Record Service (IOR Service)</a></li>-->
														</ul>
                                        </div>
                                    </div>    
                                </div><!-- end row -->
                            </li>
                        </ul>
                    </li>
                    <li><a href="#">Contact US</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>   
 
        <!-- End Side Menu -->
    </nav>
    <!-- End Navigation -->

    <div class="clearfix"></div>
<section class=" landing-page">
		<div class="landing-page-bg">
			<div class="">
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-sm-6">
							 <h1>Company Registration</h1>
							 <p>The first & foremost requirement of start-up registration is to choose the favorable business structure and also to register a company name in India.</p>
							<div>
								<ul>
									<li><img src="images/checked.svg">Private Limited Company</li>
									<li><img src="images/checked.svg">One Person Company</li>
									<li><img src="images/checked.svg">Limited Liability Partnership</li>
									<li><img src="images/checked.svg">Partnership Firm</li>
									<li><img src="images/checked.svg">Proprietorship Firm</li>
									<li><img src="images/checked.svg">MSME Registration</li>
									 
								
								     
									
								</ul>
							
						</div>
						</div>
						<div id="one" class="col-md-5 col-sm-6 col-xs-12"  >
							<div class="servicelandingForm">
							<h4 style="margin-bottom:20px;">Get in Touch</h4>
								<form action="" method="post" class="form-horizontal has-validation-callback" >
									 
									<input class="form-control inputStyles" placeholder="Name" type="email">  
									<input class="form-control inputStyles" placeholder="Email Address*" type="email" >  
									
									
									<div> 
									
									<input class="form-control inputStyles" minlength="10" maxlength="10" placeholder="Mobile Number*" >  </div>
									<div class="citySelect">
										<select id="sel1" class="form-control inputStyles"  style="margin-bottom: 0px">
											<option value="" selected="">Select City*</option>
										</select> 
									</div>
									<textarea class="form-control inputStyles"></textarea>
									  <button type="submit" class="btn defaultBtn contact_form_submit" id="contact_form_submit">Get Started</button>  
									 
									 
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		   </section>
    
     
		 
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>Company Registration – How to Register a Company in India</h2>
						 
						<p>The first & foremost requirement of start-up registration is to choose the favorable business structure and also to register a company name in India. The choice of right business structure will impact many factors from your business name, to your liability towards business, to how you file your taxes & statutory dues. The operational and financial success of the company also depends on the business structure, accordingly online company registration in India process can be followed.</p>
						
					</div>
					
				</div>
			</div>
			
			
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>How Do We Help With Company Registration</h2>
						 
						<p>We, CAONWEB is providing the online CA directory for start-up registration or say online company registration in India through professional like CA’s, CS’s and Lawyers as a ONE-STOP solution for all pre & post incorporation compliances & licensing requirements. The company registration process is cumbersome which involves legal obligation & can be compiled properly with the help of experienced professionals. Promisingly, we are providing options of the best company Registration Consultants in India who can help you in the online firm registration process in India, Company Incorporation, start-up registration , proprietorship firm registration, online company registration in India, etc.</p>
						
					</div>
					
				</div>
			</div>
			
			
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>Online company registration in india</h2>
						 
						<p>CAONWEB has developed the digital platform i.e. online service marketplace which serves as the online directory where one can find the professional according to their requirements & budget for the online company registration in India and also for start-up registration. CAONWEB provides you the option to answer your questions like how to register a company in India? Or how to do startup registration? Or how to avail the services like company Incorporation in India?</p>
						
					</div>
					
				</div>
			</div>
			
			
			<div class="container para-content">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<h2>Startup Registration</h2>
						 
						<p>startups which is also one of the significant reason behind the increasing number of startup registration in India. We, CAONWEB is providing the online directory where you can search the chartered accountant, company secretaries, tax consultants, business consultants, etc to cater to the needs of proprietorship firm registration, startup registration. The verified profiles of the professionals registered on our digital platform are ready to serve you & answer all your queries related to startup registration, online company registration in India and also how to register a company in India?</p>
						
						<p>The cost of company incorporation depends on two factors like which type of company you are getting registered. Start a Company & get online company registration in India with experts of CAONWEB with competitive Company Registration Services & fees, who can also help you in the register a company name in India & startup registration or online company registration in India</p>
						
					</div>
					
				</div>
			</div>
			 	 
		<div style="clear:both;"></div>		 
				 
				 <section class="third-section">
					<div class="container">
					<div class="row">
						<div class="col-md-6">
							<h2>Documents Required for Company Registration</h2>
					<ul>
								<li><img src="images/tick.svg ">Copy of PAN Card of directors</li>
								<li><img src="images/tick.svg ">Passport size photograph of directors</li>
								<li><img src="images/tick.svg ">Copy of Aadhaar Card/ Voter identity card of directors</li>
								<li><img src="images/tick.svg ">Copy of Rent agreement (If rented property)</li>
								<li><img src="images/tick.svg">Electricity/ Water bill (Business Place)</li>
								<li><img src="images/tick.svg">Copy of Property papers(If owned property)</li>
								<li><img src="images/tick.svg">Landlord NOC (Format will be provided)</li>
							</ul>
						</div>
						
							<div class="col-md-6">
							 
							 <iframe width="100%" height="350" src="https://www.youtube.com/embed/MnvzaRNTsqQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe> 	 
						</div> 	
						
					</div>
					</div>
				 </section>
			 	    
    
 
    
    
			 	 <div style="clear:both;"></div> 	 	 
		
    
    <section class="pricing py-5">
  <div class="container">
    <div class="row">
	<div class="col-md-12">
	<h2>Requirements for Start Up Incorporation</h2>
	</div>
      
      <div class="col-lg-3">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body">
          
            <h6 class="card-price text-center">One Person Company</h6>
            <hr>
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span>1 Shareholders</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>1 Directors</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>1 Nominee</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DIN </li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DSC</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>Minimum Authorised Share Capital 1 Lakh</li>
						 
						</ul>
			 <a href="https://www.caonweb.com/one-person-company.php" target="_blank" title="One Person Company Registration Process" class="btn btn-block btn-primary text-uppercase">Read More</a>
			 
			 
			 
			 
             
          </div>
        </div>
      </div>
      <!-- Plus Tier -->
      <div class="col-lg-3">
        <div class="card mb-5 mb-lg-0">
          <div class="card-body">
           <h6 class="card-price text-center">Private Limited Company</h6>
            <hr>
			
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span> 2 Shareholders</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> 2 Directors</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> DIN </li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> DSC</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Minimum Authorised Share Capital 1 Lakh</li>
					 
						
						</ul>
			<a href="https://www.caonweb.com/limited-liability-partnership.php" target="_blank" title="Online Private Limited Company Registration - Fees, Documents & Procedure" class="btn btn-block btn-primary text-uppercase">Read More</a>
			 
			 
             
          </div>
        </div>
      </div>
      <!-- Pro Tier -->
      <div class="col-lg-3">
        <div class="card">
          <div class="card-body">
           <h6 class="card-price text-center">Limited Liability Partnership</h6>
            <hr>
			
			
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span>2 Designated Partners</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DIN </li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>DSC</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span>Capital Contribution 10000/-</li>
						 
						</ul>
			<a href="https://www.caonweb.com/limited-liability-partnership.php" target="_blank" title="Limited Liability Partnership (LLP) Registration in India" class="btn btn-block btn-primary text-uppercase">Read More</a>
			
			
			
			
			
			
			
			
			 
             
          </div>
        </div>
      </div>
	  
	   <div class="col-lg-3">
        <div class="card">
          <div class="card-body">
           <h6 class="card-price text-center">Proprietorship</h6>
            <hr>
			
			<ul class="fa-ul">
						<li><span class="fa-li"><i class="fas fa-check"></i></span> PAN</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Adhaar</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Bank Details</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Business Details</li>
						<li><span class="fa-li"><i class="fas fa-check"></i></span> Investment minimum 1 Lakh</li>
						 
						</ul>
			 
            <a href="https://caonweb.com/proprietorship.php" target="_blank" title="How to Register a Proprietorship Firm in India" class="btn btn-block btn-primary text-uppercase">Read More</a>
          </div>
        </div>
      </div> 
	  
	  
	  
    </div>
  </div>
</section>
    
     
				 <section class=" fifth-section">
					<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>How to register a company in india</h2>
					<ul>
								<li>
									<span>1</span>
										<h6>Name approval</h6>
										<p>The first step of incorporating the company is to decide about the name of your company which will be applied to MCA for approval. Names given for approval should ideally be unique and related to business activities of the company.</p>
								</li>
								<li>
									<span>2</span>
										<h6>Application of DSC & DPIN</h6>
										<p>The next step for startup registration is to apply for Digital signature and DPIN. A digital signature is an online signature used for signing the e-forms and DPIN refer to Director's identification number issued by Registrar.</p>
								</li>
								<li>
									<span>3</span>
										<h6>MOA & AOA submission</h6>
										<p>Once the name is approved, Memorandum of association and Articles of Association needs to be prepared which includes the rules & by-laws of the company that regulates the operation of the business. Both MOA and AOA are filed with the MCA with the subscription statement for the authentication & approval.</p>
								</li>
								
								<li>
									<span>4</span>
										<h6>Prepare form & documents</h6>
										<p>Fill the application forms duly, attach the documents, get the same verified by professional then file the form to ROC then make the payment.</p>
								</li>
								
								<li>
									<span>5</span>
										<h6> Get incorporation certificate</h6>
										<p>Once all the documentation is done & form is filed with the department, registrar issues the incorporation certificate. The certificate of incorporation mentions significant information about the company such as CIN number, the name of the company, date of incorporation, etc.</p>
								</li>
								
								<li>
									<span>6</span>
										<h6>Apply for Bank account</h6>
										<p>After the receipt of incorporation certificate, you can submit the copy of Incorporation certificate, MOA, AOA and PAN along with the account opening form of the bank to open your bank account.</p>
								</li>
								 
								
								
								
								
								
								
								
										
								 
 							</ul>
						</div>
						 
						
					</div>
					</div>
				 </section>
    
     <div style="clear:both;"></div> 	 
    
    
    <section class="bg-grey faq">
    
	
		<div class="container">
		<h2>Frequently Asked Questions</h2>
		 
			 <div class='animatedParent' data-sequence='500'>
				<div class='  animated bounceInLeft slower'   data-id='1'>
				<div class="col-md-6">
					<!-- Tab -->
					<div class="panel-group accordion" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
									What Is The Minimum Requirement Of Director For One Person Company Registration?
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>The minimum requirement of directors for a one person company is one (1), additionally, there is a requirement of one member &amp; nominee.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									Which Form Of Entity Is Favourable To Start A New Business? 
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true" style="">
								<div class="panel-body">
									<p>There are various types of business entities in India such as sole proprietorship, partnership firms, Co-operative Societies, Companies, etc. The choice of suitable business structure for any particular business depends on factors like ownership, availability of capital resources, the scale of business, etc.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="https://www.caonweb.com/company-registration.php#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									How &amp; Where I Have To Apply For Online Company Registration In India?
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>The application has to be submitted online on the department portal in the prescribed format along with the applicable documents. In case of any discrepancy or approval, you will get the email from the department.</p>
								</div>
							</div>
						</div>
					</div><!-- Tab -->
					
				</div><!-- Column -->
				</div><!-- Column -->
				<div class='  animated bounceInRight slower' data-id='2'>
				<div class="col-md-6 margin-top-991-30" >
				
					<!-- Tab -->
					<div class="panel-group accordion dark" id="accordion2" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading5">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse5" aria-expanded="false" aria-controls="collapse5" class="collapsed">
									What Are The Documents Required For Online Company Registration In India? 
									</a>
								</h4>
							</div>
							<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5" aria-expanded="false" style="height: 0px;">
								<div class="panel-body">
									<p>The significant documents required for company registration are Pan Card &amp; aadhar card of directors, passport size photos of directors, conveyance deed or rent agreement, utility bills etc. along with the prescribed documents which can be prepared with the help of professional.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading6">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse6" aria-expanded="false" aria-controls="collapseTwo">
									Can Person Resident Outside India/ NRI/ Foreigner Become A Director In Private Limited Company?
									</a>
								</h4>
							</div>
							<div id="collapse6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading6" aria-expanded="true" style="">
								<div class="panel-body">
									<p>Yes, there is no restriction on person resident outside India/ NRI or Foreign National for becoming a Director in a Private Limited Company but at least one Director in the Board must be an Indian Resident.</p>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading7">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="https://www.caonweb.com/company-registration.php#collapse8" aria-expanded="false" aria-controls="collapseThree">
								Can I Incorporate Company By Myself? 
									</a>
								</h4>
							</div>
							<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7" aria-expanded="false">
								<div class="panel-body">
									<p>Yes, you can complete all the documentation of the company by yourself, but it is recommended to take help of professional as verification of the chartered accountant or company secretary is mandatory for certifying that all the required compliances have been made. The professional can also help you in post compliances of <strong>startup registration</strong> or <strong>online company registration in India.</strong></p>
								</div>
							</div>
						</div>
					</div><!-- Tab -->
				</div><!-- Column -->
				</div><!-- Column -->
				</div><!-- Column -->
			</div><!-- Row -->
		
	 
    </section>
    
    
    
    
   <section class="subscribe">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12">
			 <h2>Need Help? Call Us 24/7</h2><span>0120 - 4231116</span>
			</div>
			<div class="col-md-6 col-xs-12">
				<input type="text" placeholder="Enter Email ID"><button type="submit">SUBMIT</button>
			</div>
		</div>
	</div>
</section>   
    
    
    
  <footer class="footer page-section-pt black-bg">
 <div class="container">
  <div class="row">
      <div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Navigation</h6>
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Service</a></li>
          <li><a href="#">Team</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul>
      </div>
    </div>
    <div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Useful Link</h6>
        <ul>
          <li><a href="#">Create Account</a></li>
          <li><a href="#">Company Philosophy</a></li>
          <li><a href="#">Corporate Culture</a></li>
          <li><a href="#">Portfolio</a></li>
          <li><a href="#">Client Management</a></li>
        </ul>
      </div>
    </div>
	
	<div class="col-lg-2 col-sm-6 sm-mb-30">
      <div class="footer-useful-link footer-hedding">
        <h6 class="text-white mb-30 mt-10 text-uppercase">Services</h6>
        <ul>
          <li><a href="#">TDS Return</a></li>
          <li><a href="#">GST Registration</a></li>
          <li><a href="#">GST Filing</a></li>
          <li><a href="#">FSSAI Registration</a></li>
          <li><a href="#">FDI in India</a></li>
        </ul>
      </div>
    </div>
    
	 <div class="col-lg-4 col-sm-6 xs-mb-30">
    <h6 class="text-white mb-30 mt-10 text-uppercase">Contact Us</h6>
    <ul class="addresss-info"> 
        <li><i class="fa fa-map-marker"></i> <p>Address: E-36, Sector 8 Noida UP 201301</p> </li>
        <li><i class="fa fa-phone"></i> <a href="tel:01204231116"> <span>0120 - 4231116 </span> </a> </li>
        <li><i class="far fa-envelope"></i>Email: info@caonweb.com</li>
      </ul>
    </div> 
	
	<div style="clear:both;"></div>
      
      <div class="footer-widget mt-20">
         
          <div class="col-lg-6 col-md-6">
           <p class="mt-15"> ©Copyright <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>2019</span> <a href="#"> CAONWEB </a> All Rights Reserved </p>
          </div>
          <div class="col-lg-6 col-md-6 text-left text-md-right">
            <div class="social-icons color-hover mt-10">
             <ul> 
              <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a></li>
              <li class="social-dribbble"><a href="#"><i class="fab fa-linkedin-in"></i> </a></li>
              <li class="social-twitter"><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
              <li class="social-twitter"><a href="#"><i class="fab fa-youtube"></i></a></li>
             </ul>
           </div>
          </div>
          
      </div>
  </div> </div>
</footer>  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
	<!-- START JAVASCRIPT -->
    <!-- Placed at the end of the document so the pages load faster -->
   
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://www.caonweb.com/js/lib/bootstrap.min.js"></script>
    <script src="new-landingJS/bootstrap.min.js"></script>
    
    <!-- Bootsnavs -->
    <script src="new-landingJS/bootsnav.js"></script>
 <script type="text/javascript" src="new-landingJS/css3-animate-it.js"></script>
 
 
 
 
</body>
</html>
